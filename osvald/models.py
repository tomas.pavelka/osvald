from django.db import models

# Create your models here.
class User(models.Model):
    username = models.CharField(max_length=255)
    first_name = models.CharField(max_length=255)
    middle_name = models.CharField(max_length=255)
    surname = models.CharField(max_length=255)
    external_id = models.CharField(max_length=255)
    created_date = models.DateTimeField(
            blank=True, null=True)


    def registrer(self):
        self.created_date = timezone.now()
        self.save()

    def __str__(self):
        return self.username